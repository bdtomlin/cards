package main

import (
	"os"
	"testing"
)

const testFileName = "_deck_test_file"

func TestNewDeck(t *testing.T) {
	d := newDeck()

	if e, a := 16, len(d); a != e {
		t.Errorf("Expected deck length of %v but got %v", e, a)
	}

	if e, a := "Ace of Spades", d[0]; a != e {
		t.Errorf("Expected first card %v, but got %v", e, a)
	}

	if e, a := "Four of Clubs", d[len(d)-1]; a != e {
		t.Errorf("Expected first card %v, but got %v", e, a)
	}
}

func TestSaveToFileAndNewDeckFromFile(t *testing.T) {
	os.Remove(testFileName)
	defer os.Remove(testFileName)
	d := newDeck()
	d.saveToFile(testFileName)
	d = newDeckFromFile(testFileName)

	if e, a := 16, len(d); a != e {
		t.Errorf("Expected deck length of %v but got %v", e, a)
	}
	os.Remove(testFileName)
}
